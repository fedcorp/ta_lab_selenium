package src;


import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.junit.jupiter.api.Assertions;

public class Main {

    @Test
    public void googleSearchResponseTest () {
        String key = "Apple";
        System.setProperty("webdriver.chrome.driver" ,"src/main/resources/chromedriver_win32/chromedriver.exe");
        WebDriver driver = new ChromeDriver();

        driver.get("https://www.google.com/");
        WebElement element = driver.findElement(By.name("q"));
        element.sendKeys(key);
        element.submit();

        new WebDriverWait(driver, 20).until(d -> d.getTitle().toLowerCase().startsWith(key.toLowerCase()));
        Assertions.assertTrue(driver.getTitle() != key);
        System.out.println("Response is correct!");
        driver.quit();
    }
}
